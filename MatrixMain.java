
import java.util.Scanner;
import java.util.Random;
class Matrix{
	private int row;
	private int col;
	protected double data[][];
	private String name;
        public Matrix(){
            Random random=new Random();
            rows=random.nextInt(10)+1;
            columns=random.nextInt(10)+1;
            data=new double[rows][columns];
            int i,j;
            for (i=0;i<rows;i++){
                for(j=0;j<columns;j++){
                    double values=random.nextDouble(1000.0);
                    this.data[i][j]=values;
                }
            }
            for (i=0;i<rows;i++){
                for(j=0;j<columns;j++){
                    System.out.print(this.data[i][j]+"  ");       
                }
                System.out.println();
            }                             
        }
	public Matrix(int rows,int cols){
		row=rows;
		col=cols;
		data=new double[row][col];
	}
	public void setElement(int r,int c,double val){
		data[r][c]=val;

	}
	public double getElement(int r,int c){
		return data[r][c];
	}
	public Matrix add(Matrix mat2){
		Matrix result= new Matrix(row,col);
		int i,j;
		for(i=0;i<result.row;i++){
			for(j=0;j<result.col;j++){
				result.data[i][j]=this.data[i][j]+mat2.data[i][j];
			}
		}
		return result;
	}
	public Matrix subtract(Matrix mat2){
		Matrix result= new Matrix(row,col);
		int i,j;
		for(i=0;i<result.row;i++){
			for(j=0;j<result.col;j++){
				result.data[i][j]=this.data[i][j]-mat2.data[i][j];
			}
		}
		return result;
	}
	public Matrix multiply(Matrix mat2){
		Matrix result=new Matrix(this.row,mat2.col);
		int i,j,k;
		for(i=0;i<result.row;i++){
			for(j=0;j<result.col;j++){
				for(k=0;k<this.col;k++){
					result.data[i][j]+=this.data[i][k]*mat2.data[k][j];
				}
			}
		}
		return result;
	}
	public Matrix transpose(){
		Matrix result=new Matrix(col,row);
		for(int i=0;i<result.row;i++){
			for(int j=0;j<result.col;j++){
				result.data[i][j]=this.data[j][i];
			}
		}
		return result;
	}
	public String toString(){
		String elmt="";
		for(int i=0;i<this.row;i++){
			for(int j=0;j<this.col;j++){
				elmt+=this.data[i][j]+" ";
			}
		}
		return elmt;
	}
}
class SquareMatrix extends Matrix{
	private int order;
	public SquareMatrix(int size){
		super(size,size);
                order=size;
        }
        public boolean isSymmetric(){
            for(int i=0;i<this.order;i++){
                for(int j=0;j<this.order;j++){
                    if(super.getElement(i,j)!=super.getElement(j,i)){
                        System.out.println("Not Symmetric!!");
                        return false;
                    }                      
                }
            }
            System.out.println("Symmetric!!");
	    return true;
        }
	/*public boolean isSingular(){*/
	public double mat_trace(){
            double trace=0;
	    for(int i=0;i<this.order;i++){
                for(int j=0;j<this.order;j++){
			if(i==j){
				trace+=this.data[i][j];
			}
		}
	    }
	    return trace;
	}		
class DiagonalMatrix extends SquareMatrix{
    private double values;
    public DiagonalMatrix(int size,double values){
        
    }
            
}

		
}
public class MatrixMain{	
	public static void main(String [] args){
		Scanner obj= new Scanner(System.in);
	     /*	System.out.println("Enter the number of rows");
		int rows=obj.nextInt();
		System.out.println("Enter the number of columns");
		int cols=obj.nextInt();
		Matrix mat=new Matrix(rows,cols);
		System.out.println("Enter the values");
		int i,j;
		for (i=0;i<rows;i++){
			for(j=0;j<cols;j++){
				double val=obj.nextDouble();
				mat.setElement(i,j,val);
			}
		}
		System.out.println("The Matrix is:");
		for (i=0;i<rows;i++){
			for(j=0;j<cols;j++){
				System.out.print(mat.getElement(i,j)+" ");
			}
			System.out.println();
		}*/
		/*System.out.println("Enter the number of rows of new matrix");
		int row2=obj.nextInt();
		System.out.println("Enter the number of columns of new matrix");
		int col2=obj.nextInt();
		Matrix mat1=new Matrix(row2,col2);
		System.out.println("Enter the values");
		for (i=0;i<row2;i++){
			for(j=0;j<col2;j++){
				double val=obj.nextDouble();
				mat1.setElement(i,j,val);
			}
		}
		if(rows==row2 && cols==col2){
			Matrix sum=mat.add(mat1);
			System.out.println("The Matrix sum is:");
			for (i=0;i<rows;i++){
				for(j=0;j<cols;j++){
					System.out.print(sum.getElement(i,j)+" ");
				}
				System.out.println();
			}
			Matrix diff=mat.subtract(mat1);
			System.out.println("The Matrix Difference is:");
			for (i=0;i<rows;i++){
				for(j=0;j<cols;j++){
					System.out.print(diff.getElement(i,j)+" ");
				}
				System.out.println();		
			}
		}
		else{
			System.out.println("Matrix Addition and Subtracton are not possible!!");
		}
		
		if(cols==row2){
			Matrix prod=mat.multiply(mat1);
			System.out.println("The product of matrices are:");
			for (i=0;i<rows;i++){
				for(j=0;j<col2;j++){
					System.out.print(prod.getElement(i,j)+" ");
				}
				System.out.println();		
			}
		}
		else{
			System.out.println("Matrix Multiplication is not possible!!");
		}
		Matrix trans=mat.transpose();
		System.out.println("The Transpose of matrix is:");
		for (i=0;i<cols;i++){
			for(j=0;j<rows;j++){
				System.out.print(trans.getElement(i,j)+" ");
			}
			System.out.println();		
		}
		String toStr=mat.toString();
		System.out.println("Conversion using toString:");
		System.out.println(toStr);*/	
                System.out.println("Enter the Order of Square Matrix:");
                int n=obj.nextInt();
		SquareMatrix sq_mat=new SquareMatrix(n);
		System.out.println("Enter the values");
		int i,j;
		for (i=0;i<n;i++){
			for(j=0;j<n;j++){
				double val=obj.nextDouble();
				sq_mat.setElement(i,j,val);
			}
		}
		System.out.println("The Matrix is:");
		for (i=0;i<n;i++){
			for(j=0;j<n;j++){
				System.out.print(sq_mat.getElement(i,j)+" ");
			}
			System.out.println();
		}
		sq_mat.isSymmetric();
                System.out.println("Trace of the matrix is "+sq_mat.mat_trace());
                
                //cycle1-qn 2              
                 System.out.print("Enter the row,column and values for the matrix as a single line separated by space:");
                 String val=obj.nextLine();
                 String valu[]=val.split(" ");
                 int len=valu.length;
                 int row=Integer.parseInt(valu[0]);
                 int col=Integer.parseInt(valu[1]);
                 Matrix mat=new Matrix(row,col);
                 int i,j;
                 int start=2;
                 while (start<len){
                 for(i=0;i<row;i++){
                     for (j=0;j<col;j++){
                         double values=Double.parseDouble(valu[start]);
                         mat.setElement(i, j,values);
                         start++;
                     }
                 }
                 }
                  for(i=0;i<row;i++){
                     for (j=0;j<col;j++){
                         System.out.print(mat.getElement(i, j)+" ");
                     }
                     System.out.println();
                  }
                //cycle1-qn 4
                Matrix mat_rand=new Matrix();
	}
}
 
